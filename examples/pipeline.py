#!/usr/bin/env python3

import gi
import time
gi.require_version('Gst', '1.0')
from gi.repository import Gst
Gst.init(None)
pipeline = Gst.parse_launch('videotestsrc name=vidsrc0 ! aasink')
pipeline.set_state(Gst.State.PLAYING)
time.sleep(4)
video_src = pipeline.get_by_name('vidsrc0')
video_src.set_property('pattern', 'ball')
time.sleep(4)
video_src.set_property('pattern', 'smpte')
time.sleep(4)
pipeline.set_state(Gst.State.NULL)
