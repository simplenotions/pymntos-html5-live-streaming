#!/usr/bin/env python3

import gi
import time
gi.require_version('Gst', '1.0')
from gi.repository import Gst
Gst.init(None)
pipeline = Gst.Pipeline.new('demo')
video_src = Gst.ElementFactory.make('videotestsrc')
video_sink = Gst.ElementFactory.make('aasink')
audio_src = Gst.ElementFactory.make('audiotestsrc')
audio_src.set_property('wave', 'pink-noise')
audio_sink = Gst.ElementFactory.make('autoaudiosink')
for elem in (video_src, video_sink, audio_src, audio_sink):
    pipeline.add(elem)
video_src.link(video_sink)
audio_src.link(audio_sink)
pipeline.set_state(Gst.State.PLAYING)
time.sleep(5)
video_src.set_property('pattern', 'ball')
time.sleep(5)
pipeline.set_state(Gst.State.NULL)
