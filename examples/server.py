#!/usr/bin/env python3

import logging
logging.basicConfig(level=logging.INFO)
from datetime import datetime, timezone
import socket
from threading import Thread
import gi
gi.require_version('Gst', '1.0')
from gi.repository import Gst

from flask import Flask, Response, render_template, request, jsonify
app = Flask(__name__)

@app.route('/')
def index():
    return '''
    <video muted autoplay controls width="640" height="360">
        <source src="/live" type="video/webm">
    </video>
    '''

def stream_video(stream_socket):
    while True:
        yield stream_socket.recv(256000)

@app.route('/live')
def livestream():
    utcnow = datetime.now(timezone.utc).strftime("%Y-%m-%d'T'%H:%M:%SZ")
    video_stream = socket.socket()
    video_stream.connect(('127.0.0.1', 9001))
    return Response(
            stream_video(video_stream),
            status=200,
            headers={
                'Date': utcnow,
                'Connection': 'close',
                'Cache-Control': 'private',
                'Content-Type': 'video/webm',
                'Server': 'CustomStreamer/0.0.1'
            })

class GStreamerThread(Thread):
    def __init__(self, **kwargs):
        Thread.__init__(self)
        Gst.init(None)
        self.pipeline = None
        base_pipeline = (
            'videotestsrc '
            '! video/x-raw,width=640,height=360 '
            '! videoconvert '
            '! vaapivp8enc bitrate=2048 keyframe-period=60 '
            '! webmmux streamable=true name=webmux '
            '! tcpserversink host=127.0.0.1 port=9001  '
            'audiotestsrc wave=pink-noise '
            '! vorbisenc '
            '! webmux.  '
        )
        self.pipeline = Gst.parse_launch(base_pipeline)

    def run(self):
        self.pipeline.set_state(Gst.State.PLAYING)

if __name__ == '__main__':
    gstreamer_thread = GStreamerThread()
    gstreamer_thread.start()
    app.run(host='0.0.0.0', port=8080, threaded=True)
