#!/usr/bin/env python3

import os
import gi
import time
gi.require_version('Gst', '1.0')
from gi.repository import Gst, GObject
Gst.init(None)
launch_str = (
    'videotestsrc name=vidsrc0 ! video/x-raw,width=1280,height=720,framerate=30/1 '
    '! x264enc speed-preset=ultrafast tune=zerolatency bitrate=2048 key-int-max=60 '
    '! video/x-h264,profile=main '
    '! flvmux name=mux streamable=true '
    '! rtmpsink location="rtmp://a.rtmp.youtube.com/live2/{}" '
    'audiotestsrc wave=pink-noise ! voaacenc ! mux.  '
).format(os.environ['YOUTUBE_KEY'])
pipeline = Gst.parse_launch(launch_str)
pipeline.set_state(Gst.State.PLAYING)
time.sleep(10)
video_src = pipeline.get_by_name('vidsrc0')
video_src.set_property('pattern', 'ball')
time.sleep(10)
pipeline.set_state(Gst.State.NULL)
