#!/usr/bin/env python3
import gi
import time
gi.require_version('Gst', '1.0')
from gi.repository import Gst, GObject
Gst.init(None)
pipeline = Gst.parse_launch('audiotestsrc wave=pink-noise ! autoaudiosink')
pipeline.set_state(Gst.State.PLAYING)
time.sleep(5)
pipeline.set_state(Gst.State.NULL)
